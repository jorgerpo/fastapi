image: "docker:19.03.5"

stages:
  - static_analysis
  - security_static_analysis
  - test
  - build
  - integration_test
  - security_analysis
  - publish
  - deployment

variables:
  CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  CONTAINER_DEVELOP_IMAGE: $CI_REGISTRY_IMAGE:develop
  CONTAINER_LATEST_IMAGE: $CI_REGISTRY_IMAGE:latest
  CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG

flake8:
  stage: static_analysis
  image: "registry.gitlab.com/nevrona/public/poetry-docker:1.0.2"
  variables:
    INVOKE_RUN_SHELL: /bin/ash
    POETRY_VIRTUALENVS_CREATE: 0
  before_script:
    - poetry install
  script:
    - inv flake8
  except:
    - master
    - tags

pydocstyle:
  stage: static_analysis
  image: "python:3.8.1-alpine3.11"
  variables:
    INVOKE_RUN_SHELL: /bin/ash
  script:
    - pip install --no-cache-dir pydocstyle invoke
    - inv pydocstyle
  except:
    - master
    - tags

hadolint:
  stage: static_analysis
  services:
    - docker:19.03.5-dind
  variables:
    HADOLINT_IMAGE: hadolint/hadolint:latest
  script:
    - docker run --rm -i "$HADOLINT_IMAGE" < Dockerfile
  except:
    - master
    - tags

bandit:
  stage: security_static_analysis
  image: "python:3.8.1-alpine3.11"
  variables:
    INVOKE_RUN_SHELL: /bin/ash
  script:
    - pip install --no-cache-dir bandit invoke
    - inv bandit
  except:
    - master
    - tags

tests:
  stage: test
  image: "registry.gitlab.com/nevrona/public/poetry-docker:1.0.2"
  variables:
    INVOKE_RUN_SHELL: /bin/ash
    POETRY_VIRTUALENVS_CREATE: 0
  before_script:
    - poetry install
  script:
    - inv tests
  except:
    - master
    - tags

build test:
  stage: build
  services:
    - docker:19.03.5-dind
  artifacts:
    paths:
      - .image
      - .image_tag
    expire_in: 1 hour
  script:
    - IMAGE="$CONTAINER_TEST_IMAGE"
    - printf "%s" "$IMAGE" > .image_tag
    - app_version="$(grep version pyproject.toml | cut -d= -f2 | tr -d '" \n')"
    - internal_version="${app_version}-${CI_COMMIT_SHORT_SHA}"
    - sed -i "s/''/'${internal_version}'/" app_skel/__init__.py
    - docker build --label "org.opencontainers.image.created"="$(date -Iseconds)" --label "org.opencontainers.image.revision"="$CI_COMMIT_SHA" --label "org.opencontainers.image.version"="$app_version" --label "org.opencontainers.image.ref.name"="test" --compress --pull --rm --tag "$IMAGE" .
    - docker image save -o .image "$IMAGE"
  except:
    - develop
    - master
    - tags

build develop:
  stage: build
  services:
    - docker:19.03.5-dind
  artifacts:
    paths:
      - .image
      - .image_tag
    expire_in: 1 hour
  script:
    - IMAGE="$CONTAINER_DEVELOP_IMAGE"
    - printf "%s" "$IMAGE" > .image_tag
    - app_version="$(grep version pyproject.toml | cut -d= -f2 | tr -d '" \n')"
    - internal_version="${app_version}-${CI_COMMIT_SHORT_SHA}"
    - sed -i "s/''/'${internal_version}'/" app_skel/__init__.py
    - docker build --label "org.opencontainers.image.created"="$(date -Iseconds)" --label "org.opencontainers.image.revision"="$CI_COMMIT_SHA" --label "org.opencontainers.image.version"="$app_version" --label "org.opencontainers.image.ref.name"="develop" --compress --pull --rm --tag "$IMAGE" .
    - docker image save -o .image "$IMAGE"
  only:
    - develop

build latest:
  stage: build
  services:
    - docker:19.03.5-dind
  artifacts:
    paths:
      - .image
      - .image_tag
    expire_in: 1 hour
  script:
    - IMAGE="$CONTAINER_LATEST_IMAGE"
    - printf "%s" "$IMAGE" > .image_tag
    - app_version="$(grep version pyproject.toml | cut -d= -f2 | tr -d '" \n')"
    - internal_version="${app_version}-${CI_COMMIT_SHORT_SHA}"
    - sed -i "s/''/'${internal_version}'/" app_skel/__init__.py
    - docker build --label "org.opencontainers.image.created"="$(date -Iseconds)" --label "org.opencontainers.image.revision"="$CI_COMMIT_SHA" --label "org.opencontainers.image.version"="$app_version" --label "org.opencontainers.image.ref.name"="$app_version" --compress --pull --rm --tag "$IMAGE" .
    - docker image save -o .image "$IMAGE"
  only:
    - master

build tag:
  stage: build
  services:
    - docker:19.03.5-dind
  artifacts:
    paths:
      - .image
      - .image_tag
    expire_in: 1 hour
  script:
    - IMAGE="$CONTAINER_RELEASE_IMAGE"
    - printf "%s" "$IMAGE" > .image_tag
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - docker pull "$CONTAINER_LATEST_IMAGE"
    - docker tag "$CONTAINER_LATEST_IMAGE" "$IMAGE"
    - docker image save -o .image "$IMAGE"
  only:
    - tags

run_app:
  stage: integration_test
  services:
    - docker:19.03.5-dind
  allow_failure: true
  before_script:
    - docker network create integration_test
  script:
    - IMAGE="$(cat .image_tag)"
    - printf "%s\n" "$IMAGE"
    - docker image load -i .image
    - docker inspect "$IMAGE"
    - test 1 -eq 0  # make it fail for now
  after_script:
    - docker network rm integration_test
  except:
    - master
    - tags

# https://www.objectif-libre.com/en/blog/2018/07/26/scanning-docker-images-with-clair-and-gitlab/
# https://hub.docker.com/r/objectiflibre/clair-scanner
container_scanning:
  stage: security_analysis
  services:
    - docker:19.03.5-dind
  artifacts:
    reports:
      container_scanning: gl-container-scanning-report.json
  variables:
    CLAIR_DB_IMAGE_TAG: "latest"
    CLAIR_DB_IMAGE: "arminc/clair-db:$CLAIR_DB_IMAGE_TAG"
    CLAIR_LOCAL_IMAGE_TAG: "latest"
    CLAIR_LOCAL_IMAGE: "arminc/clair-local-scan:$CLAIR_LOCAL_IMAGE_TAG"
    CLAIR_SCANNER_IMAGE_TAG: "latest"
    CLAIR_SCANNER_IMAGE: "objectiflibre/clair-scanner:$CLAIR_SCANNER_IMAGE_TAG"
  before_script:
    - apk update && apk add coreutils
    - docker network create scanning
    - docker run -d --net=scanning --name db "$CLAIR_DB_IMAGE"
    - sleep 10
    - docker run -d --net=scanning --name clair --link db:postgres "$CLAIR_LOCAL_IMAGE"
    - sleep 10
  script:
    - IMAGE="$(cat .image_tag)"
    - printf "%s\n" "$IMAGE"
    - docker image load -i .image
    - docker run --net=scanning --name=scanner --link=clair:clair -v '/var/run/docker.sock:/var/run/docker.sock' "$CLAIR_SCANNER_IMAGE" --clair="http://clair:6060" --ip="scanner" -r report.json "$IMAGE"
    - docker container cp scanner:report.json ./gl-container-scanning-report.json
    - cat ./gl-container-scanning-report.json
  after_script:
    - docker stop scanner clair db
    - docker rm -vf scanner clair db
    - docker network rm scanning
  except:
    refs:
      - master
      - tags
    variables:
      - $CONTAINER_SCANNING_DISABLED

publish:
  stage: publish
  services:
    - docker:19.03.5-dind
  script:
    - IMAGE="$(cat .image_tag)"
    - printf "%s\n" "$IMAGE"
    - docker image load -i .image
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - docker push "$IMAGE"
  only:
    - develop
    - master
    - tags
