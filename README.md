# FastAPI Skeleton

Generic skeleton for a FastAPI application using Uvicorn for development.

## Getting Started
 
These instructions will guide you on how to use this skeleton.

### Usage

First clone this repo: `git clone git@gitlab.com:nevrona/public/skels/fastapi.git`

After that search and replace `app_skel` with your application name. Also search and replace `AEP` with your application environment prefix (usually a three characters code).

You must change the vendor `LABEL org.opencontainers.image.vendor` in the `Dockerfile`, and all the necessary settings to suit your needs in the `pyproject.toml`, the `Dockerfile` and check the `.gitlab-ci.yml`, there are some predefined jobs such as run tests and build and publish Docker images so adjust them to your needs (the Docker images in this repo are not useful and are used just to verify that the CI is working correctly).

Also you have to configure the `global_settings`, this skeleton came with settings examples, check them inside `app_skel/conf`. The `APP_TITTLE` setting is required in order to run the app. 

To make your life easier, this skeleton came also with a series of tasks using `invoke`, if you don't know it check the [documentation](https://www.pyinvoke.org/).

For example you can run `inv lint flake8` to lint your code, this skeleton have a lot of flake plugins. To view all the tasks check the file `tasks.py` or run `inv -l`. And to view all the flake plugins and other libraries used check the `pyproject.toml`.

When you have all set up you can run your application with `inv runserver` and have fun :).

#### No database

If you are not planning to use a database, remove the following modules:

* crud
* db
* models

And the *shortcuts* file in the *api* module and the settings tagged as database from the global_settings file.

## License

*FastAPI Skeleton* is made by [Nevrona S. A.](https://nevrona.org) under `MPL-2.0`. You are free to use, share, modify and share modifications under the terms of that [license](LICENSE).

**Neither the name of Nevrona S. A. nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.**
