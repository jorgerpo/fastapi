"""Common tasks for Invoke."""

from invoke import task

# Set the application name
APP_NAME: str = 'app_skel'
# Set your registry URL
REGISTRY = 'registry.gitlab.com/nevrona/public/skels/fastapi'


@task(
    default=True,
    help={
        'development': 'run a development server'
    }
)
def runserver(ctx, development=False):
    """Run a development or production server."""
    if development:
        ctx.run(
            f'uvicorn {APP_NAME}.app:app --reload',
            echo=True,
            pty=True,
            env={
                'AEP_DEVELOPMENT_MODE': 'true',
                'AEP_ALLOWED_HOSTS': '127.0.0.1',
            }

        )
    else:
        ctx.run(
            f'gunicorn --config {APP_NAME}/conf/gunicorn.py --pythonpath "$(pwd)"'
            f' {APP_NAME}.app:app',
            echo=True,
        )


@task
def flake8(ctx):
    """Run flake8 with proper exclusions."""
    ctx.run(
        f'flake8 --exclude migrations,local_settings.py {APP_NAME}/',
        echo=True,
    )


@task
def pydocstyle(ctx):
    """Run pydocstyle with proper exclusions."""
    cmd = f'find {APP_NAME}/'
    ctx.run(
        cmd + ' -type f \\( -path "*/migrations/*" -o -path "*/local_settings.py" \\) '
              '-prune -o -name "*.py" -exec pydocstyle --explain "{}" \\+',
        echo=True,
    )


@task
def bandit(ctx):
    """Run bandit with proper exclusions."""
    ctx.run(f'bandit -i -r --exclude local_settings.py {APP_NAME}/', echo=True)


@task
def lint_docker(ctx):
    """Lint Dockerfile."""
    ctx.run('sudo docker run --rm -i hadolint/hadolint < Dockerfile', echo=True,
            pty=True, echo_stdin=False)


@task(flake8, pydocstyle, bandit, lint_docker)
def lint(ctx):
    """Lint code and static analysis."""


@task
def build(ctx, tag='latest', for_tests=False):
    """Build Docker image."""
    cmd = ['sudo', 'docker', 'build', '--compress', '--pull', '--rm', '--tag',
           f'{REGISTRY}:{tag}']
    if for_tests:
        cmd.extend(['--build-arg', 'BUILD_FOR_TESTS=1'])
    ctx.run(f'{" ".join(cmd)} .', echo=True, pty=True, echo_stdin=False)


@task
def clean(ctx):
    """Remove all temporary and compiled files."""
    remove = (
        'build',
        'dist',
        '*.egg-info',
        '.coverage',
        'cover',
        'htmlcov',
    )
    ctx.run(f'rm -vrf {" ".join(remove)}', echo=True)
    ctx.run('find . -type d -name "__pycache__" -exec rm -rf "{}" \\+', echo=True)
    ctx.run('find . -type f -name "*.pyc" -delete', echo=True)


@task
def tests(ctx):
    """Run tests."""
    ctx.run('nose2 -v')
