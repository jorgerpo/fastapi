"""FastAPI application."""

from fastapi import FastAPI

from ..api.routing import build_prefix
from ..conf import settings
from ..utils.project import check_settings
from ..utils.project import get_project_internal_version
from ..utils.project import get_project_version

check_settings()

description = f'{settings.APP_DESCRIPTION} v{get_project_version()}'
if settings.DEBUG or settings.DEVELOPMENT_MODE:
    description = f'{description} ({get_project_internal_version()})'
if settings.DEVELOPMENT_MODE:
    description = f'{description} (DEV MODE)'

oauth2_redirect_url = build_prefix(f'{settings.DOCS_URL_PATH}/oauth2-redirect')

app = FastAPI(
    title=settings.APP_TITLE,
    description=description,
    version=get_project_version(),
    debug=settings.DEBUG,
    openapi_url=build_prefix(settings.OPENAPI_URL_PATH),
    docs_url=None,
    redoc_url=None,
    swagger_ui_oauth2_redirect_url=oauth2_redirect_url,
)
