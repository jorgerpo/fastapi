"""Application routers."""

import os
from importlib import import_module
from pathlib import Path

from .fastapi import app
from ..api.root import router as api_router_root
from ..api.routing import build_prefix
from ..conf import settings
from ..utils.project import get_package_name

# Add root API
app.include_router(api_router_root, prefix=build_prefix(''))

# Find and add endpoints
package_name = get_package_name()
api_path = Path(os.path.join(settings.BASE_DIR, 'api'))
for version_path in api_path.glob('v*/'):
    if version_path.is_dir():
        module_urls = import_module(f'.api.{version_path.name}.urls', package_name)
        api_router = getattr(module_urls, 'api_router')
        version_prefix = getattr(module_urls, 'version_prefix')
        app.include_router(api_router, prefix=build_prefix(version_prefix))

if settings.DEVELOPMENT_MODE:
    # Mount static assets route
    from fastapi.staticfiles import StaticFiles

    static_dir = os.path.join(settings.BASE_DIR, 'static')
    app.mount(
        build_prefix(settings.STATIC_URL_PATH),
        StaticFiles(directory=static_dir),
        name='static',
    )
