"""Application events."""

import logging

from .fastapi import app
from ..conf import settings
from ..utils.project import get_project_internal_version
from ..utils.project import get_project_version

logger = logging.getLogger(__name__)


@app.on_event('startup')
async def startup() -> None:
    """Initialize the application."""
    version, internal_version = get_project_version(), get_project_internal_version()
    logger.info('Starting %s (%s) v%s (%s)...', settings.APP_DESCRIPTION,
                settings.APP_IDENTIFIER, version, internal_version)


@app.on_event('shutdown')
async def shutdown() -> None:
    """Finalize the application."""
    # Write whatever you need to shut down the application, such as disconnecting
    # from the database
