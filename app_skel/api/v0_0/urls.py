"""API v0.0 URLs.

You can leave this file as-is for any other API version, it works auto-magically.
"""

import os

from fastapi import APIRouter

from ..routing import find_and_add_endpoints

version_path_name = os.path.basename(os.path.dirname(os.path.abspath(__file__)))
version_prefixed = version_path_name.replace('_', '.')
version_prefix = f'/{version_prefixed}'  # Required for auto-routing

api_router = APIRouter()
find_and_add_endpoints(api_router, version_prefixed[1:])
