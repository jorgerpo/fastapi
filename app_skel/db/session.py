"""Database connection session."""

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from ..conf import settings


def get_engine() -> 'sqlalchemy.engine.Engine':
    """Get the database engine."""
    # ToDo: find the `timeout` parameter, I couldn't find any (pool_timeout
    #  doesn't work for non-pools)
    kwargs = {
        'echo': True if settings.DEVELOPMENT_MODE else False,
    }
    if settings.DATABASE_URI.startswith('sqlite'):
        kwargs['connect_args'] = {'check_same_thread': False}
    return create_engine(settings.DATABASE_URI, **kwargs)


def get_session(engine: 'sqlalchemy.engine.Engine') -> sessionmaker:
    """Get the database session for the given engine."""
    return sessionmaker(autoflush=False, bind=engine)
