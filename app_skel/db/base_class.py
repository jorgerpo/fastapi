"""Base class for db models."""

from typing import TypeVar

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.declarative import declared_attr


class AutoNameBase:
    """Db model base class.

    Auto-generate table name.
    """

    @declared_attr
    def __tablename__(self) -> str:
        """Generate the model table name as the model class name."""
        return self.__name__.lower()


Base = declarative_base(cls=AutoNameBase)
# Type hinting variable for db model instances
TBase = TypeVar('TBase', bound=Base)
