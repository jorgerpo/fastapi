"""Function to initialise the dbs without migrations."""

from .base_class import Base
from .session import get_engine


def init_without_migrations():
    """Initialise db without migrations."""
    Base.metadata.create_all(bind=get_engine())
