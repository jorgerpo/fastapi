"""Expose database related classes and functions."""

# Import all the models, so that Base has them before being imported by Alembic
# (if used)
from .base_class import Base  # noqa
from .base_class import TBase  # noqa
from .initialization import init_without_migrations
from .session import get_engine
from .session import get_session
# noinspection PyUnresolvedReferences
from ..models import *  # noqa

SessionLocal = get_session(get_engine())

__all__ = (
    'Base',
    'TBase',
    'init_without_migrations',
    'SessionLocal',
)
