"""Local settings file to run tests. Rename to `local_settings.py`.

Avoid using a file like this in production!
"""
# flake8: noqa

DEBUG = True
DEVELOPMENT_MODE = False
ENABLE_HTTPS_REDIRECT = False
