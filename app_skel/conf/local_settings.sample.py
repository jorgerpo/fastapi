"""Local settings sample file for development. Rename to `local_settings.py`.

Avoid using a file like this in production in favor of env vars.
"""
# flake8: noqa

DEBUG = DEVELOPMENT_MODE = True
ENABLE_HTTPS_REDIRECT = False
