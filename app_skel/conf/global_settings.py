"""Settings for app_skel."""

import os
import typing

# Points to the application root directory
BASE_DIR: str = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Set the environment prefix
ENV_PREFIX = 'AEP'

# Development mode: this mode sets ... for
# development, enables debug, sets log level to debug.
_development_mode: str = os.getenv(f'{ENV_PREFIX}_DEVELOPMENT_MODE') or 'false'
DEVELOPMENT_MODE: bool = True if _development_mode.lower() == 'true' else False

# App title and description shown in open API documentation
# Title is required.
APP_TITLE: str = os.getenv(f'{ENV_PREFIX}_APP_TITLE', '')
APP_DESCRIPTION: str = os.getenv(f'{ENV_PREFIX}_APP_DESCRIPTION', '')

# Application identifier such as when in a cluster (it's world readable!)
APP_IDENTIFIER: str = os.getenv(f'{ENV_PREFIX}_APP_IDENTIFIER') or 'app_skel'

# Prefix for your API, such as /api/app or /app (must begin with slash)
_api_prefix: typing.Optional[str] = os.getenv(f'{ENV_PREFIX}_API_PREFIX')
API_PREFIX: str = _api_prefix.rstrip('/') if _api_prefix else ''
# Misc URLs (must begin with slash)
_openapi_url: typing.Optional[str] = os.getenv(f'{ENV_PREFIX}_OPENAPI_URL_PATH')
OPENAPI_URL_PATH: str = _openapi_url.rstrip('/') if _openapi_url else '/openapi.json'
_docs_url: typing.Optional[str] = os.getenv(f'{ENV_PREFIX}_DOCS_URL_PATH')
DOCS_URL_PATH: str = _docs_url.rstrip('/') if _docs_url else '/docs'
_redoc_url: typing.Optional[str] = os.getenv(f'{ENV_PREFIX}_REDOC_URL_PATH')
REDOC_URL_PATH: str = _redoc_url.rstrip('/') if _redoc_url else '/redoc'
_static_url: typing.Optional[str] = os.getenv(f'{ENV_PREFIX}_STATIC_URL_PATH')
STATIC_URL_PATH: str = _static_url.rstrip('/') if _static_url else '/static'

# Application allowed hosts: used to check the Host header and CORS. The first
# one is considered the main one.
_allowed_hosts: str = os.getenv(f'{ENV_PREFIX}_ALLOWED_HOSTS') or ''
ALLOWED_HOSTS: typing.Tuple[str] = tuple(
    host.strip() for host in _allowed_hosts.split(',') if host
)

# Logging
_loglevel_raw: typing.Optional[str] = os.getenv(f'{ENV_PREFIX}_LOGLEVEL')
_loglevel: str = _loglevel_raw.upper() if _loglevel_raw else ''
if _loglevel in ('DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'):
    LOGLEVEL: str = _loglevel_raw
else:
    LOGLEVEL: str = 'DEBUG' if DEVELOPMENT_MODE else 'INFO'
LOGGING: dict = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '{levelname} {asctime} p:{process:d} t:{thread:d} '
                      '[{name}.{funcName}:{lineno:d}] {message}',
            'style': '{',
        },
        'simple': {
            'format': '{levelname} {asctime} {message}',
            'datefmt': '%Y-%m-%d %H:%M:%S',
            'style': '{',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
    },
    'loggers': {
        'app_skel': {
            'handlers': ['console'],
            'level': LOGLEVEL,
        },
    },
}

# Debug
_debug = os.getenv(f'{ENV_PREFIX}_DEBUG', 'false')
DEBUG: bool = True if DEVELOPMENT_MODE else (True if _debug.lower() == 'true' else False)

# Just for dev only, bypass using local_settings
_redir: typing.Optional[str] = os.getenv(f'{ENV_PREFIX}_ENABLE_HTTPS_REDIRECT')
ENABLE_HTTPS_REDIRECT: bool = False if DEVELOPMENT_MODE else True
if _redir:
    ENABLE_HTTPS_REDIRECT = True if _redir.lower() == 'true' else False

# External static URL for serving statics (must be a complete URL,
# including protocol, FQDN and path, ending without slash). This setting
# overrides STATIC_URL_PATH.
_static_url: typing.Optional[str] = os.getenv(f'{ENV_PREFIX}_STATIC_URL')
STATIC_URL: str = _static_url.rstrip('/') if _static_url else ''

# Allow CORS for a local frontend running in port 3000 (thus the rule is
# http://127.0.0.1:3000).
# Defaults to true for development mode, false otherwise.
_allow_front: str = os.getenv(f'{ENV_PREFIX}_ALLOW_FRONTEND_LOCAL') or ''
_allow_frontend_local: bool = True if _allow_front.lower() == 'true' else False
ALLOW_FRONTEND_LOCAL: bool = True if DEVELOPMENT_MODE else _allow_frontend_local

# Set the allowed origins list
ALLOWED_ORIGINS: typing.List[str] = [
    'http://127.0.0.1:3000',  # Frontend, for development
] if ALLOW_FRONTEND_LOCAL else []
# HTTPS protocol hardcoded :)
ALLOWED_ORIGINS.extend(f'https://{host}' for host in ALLOWED_HOSTS)

# Database settings
DATABASE_NAME: str = os.getenv(f'{ENV_PREFIX}_DATABASE_NAME') or 'db.sqlite3'
DATABASE_USER: typing.Optional[str] = os.getenv(f'{ENV_PREFIX}_DATABASE_USER')
DATABASE_PASSWORD: typing.Optional[str] = os.getenv(f'{ENV_PREFIX}_DATABASE_PASSWORD')
DATABASE_HOST: str = os.getenv(f'{ENV_PREFIX}_DATABASE_HOST') or '127.0.0.1'
DATABASE_PORT: str = os.getenv(f'{ENV_PREFIX}_DATABASE_PORT') or '26257'
DATABASE_PARAMS: str = os.getenv(f'{ENV_PREFIX}_DATABASE_PARAMS') or ''
# Set SQLAlchemy Database URI
if DATABASE_USER:
    # see: https://www.cockroachlabs.com/docs/stable/connection-parameters.html
    if DATABASE_PASSWORD:
        local_part = f'{DATABASE_USER}:{DATABASE_PASSWORD}'
    else:
        local_part = DATABASE_USER
    DATABASE_URI: str = (f'cockroachdb://{local_part}@{DATABASE_HOST}:{DATABASE_PORT}/'
                         f'{DATABASE_NAME}?{DATABASE_PARAMS}')
else:
    DATABASE_URI: str = f'sqlite:///./{DATABASE_NAME}'

##############################################################################
# DO NOT ADD SETTINGS AFTER THIS LINE
##############################################################################

# As default, all settings listed in this file are mandatory, except the ones
# indicated as optional below. Settings are checked when the app starts.
OPTIONAL_SETTINGS = {
    'API_PREFIX',
    'APP_DESCRIPTION',
    'STATIC_URL',
}
