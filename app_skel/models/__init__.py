"""Expose models."""

from sqlalchemy_utils import force_auto_coercion

# Assign automatic data type coercion for all classes which are of certain type
# This needs to be done before importing models.
force_auto_coercion()

# Import all models here to leave them ready for migrations and the app

# Expose models below
__all__ = (
)
