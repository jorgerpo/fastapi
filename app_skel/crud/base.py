"""Base classes for CRUD."""

import typing
from abc import ABC

from pydantic import BaseModel
from sqlalchemy import and_
from sqlalchemy.orm import Query
from sqlalchemy.orm import Session

from ..db import Base
from ..db import TBase


class Crud(ABC):
    """Manager base class for db models.

    Extend and define class property `model` with the corresponding db model
    class.
    """

    __slots__ = (
        'db',
        '_db_obj',
    )

    model: TBase

    def __init__(self, db: Session, *, db_obj: typing.Optional[Base] = None):
        """Manage db models.

        :param db: Request Session db.
        :param db_obj: [Optional] Database object to manage.
        """
        self.db: Session = db
        self.db_obj: typing.Optional[Base] = db_obj
        if not hasattr(self, 'model'):
            raise AttributeError('Please define the "model" attribute')

    @property
    def db_obj(self) -> typing.Optional[Base]:
        """Get database object."""
        return self._db_obj

    @db_obj.setter
    def db_obj(self, obj: Base) -> None:
        """Set database object."""
        self._db_obj = obj

    def _create_before_save(self, data: BaseModel) -> None:
        """Additional create function for expansion before saving the object.

        Implement this method to do additional actions during creation process.
        """

    def _create_after_save(self, data: BaseModel) -> None:
        """Additional create function for expansion after saving the object.

        Implement this method to do additional actions during creation process.
        """

    def create(self, data: BaseModel) -> Base:
        """Create a new db object.

        Created object is stored in this object.

        :param data: Input data schema model.
        :return: DB object.
        """
        self.db_obj: Base = self.model(**data.dict())
        self._create_before_save(data)
        self.db.add(self.db_obj)
        self.db.commit()
        self.db.refresh(self.db_obj)
        self._create_after_save(data)
        return self.db_obj

    def read(
            self,
            *,
            offset: int = 0,
            limit: int = 20,
            one: bool = False,
            one_or_none: bool = False,
            filters: typing.Optional[typing.List] = None,  # Don't know which type is
            filter_combination=None,  # Don't know which type is
            joins: typing.List[any] = None,  # Don't know which type is
            query: typing.Optional[Query] = None,
            return_query: bool = False,
    ) -> typing.Union[None, Base, typing.List[Base], Query]:
        """Retrieve one or more objects from the db if any or the query.

        If `one` is True, retrieved object is stored in this object.

        :param offset: Query offset (only applied when querying several values).
        :param limit: Objects amount limit (only applied when querying several
                      values).
        :param one: [Optional] Retrieve only one object or raise exception.
        :param one_or_none: [Optional] Retrieve only one object (or None) or raise
                            exception if multiple results found.
        :param filters: [Optional] Search filters as a list.
        :param filter_combination: [Optional] Filter combination function from
                                   SQLAlchemy, such as `or_`, `and_`, etc
                                   (defaults to `and_`).
        :param joins: [Optional] List of joins to apply to the query.
        :param query: [Optional] Query to execute and to which apply given
                      parameters if any (joins, filters, one, offset, limit).
                      Use this parameter to execute a custom query.
        :param return_query: [Optional] Instead of executing the query, return it.
                             This means that query executors are not applied
                             (such as `one` or `one_or_none`).

        :return: One or more db objects if any or the query.

        :raises sqlalchemy.orm.exc.MultipleResultsFound: One object required
                but many found.
        """
        if query is None:
            query = self.db.query(self.model)

        if joins:
            for join in joins:
                query = query.join(join)

        if filters:
            filter_combination = filter_combination if filter_combination else and_
            query = query.filter(filter_combination(*filters))

        if return_query:
            return query
        elif one_or_none:
            values = query.one_or_none()
            self.db_obj = values
        elif one:
            values = query.one()
            self.db_obj = values
        else:
            query = query.offset(offset).limit(limit)
            values = query.all()

        return values

    def _update_before_save(self, data: typing.Optional[BaseModel]) -> None:
        """Additional update function for expansion before saving the object.

        Implement this method to do additional actions during update process.
        """

    def _update_after_save(self, data: typing.Optional[BaseModel]) -> None:
        """Additional update function for expansion after saving the object.

        Implement this method to do additional actions during update process.
        """

    def update(self, data: typing.Optional[BaseModel] = None) -> Base:
        """Update an existing db object.

        Note that the object must be already fetched, and will be properly updated.

        :param data: [Optional] Input data schema model.

        :return: DB object.
        """
        if self.db_obj is None:
            raise ValueError('db_obj not set')

        if data:
            update_data = data.dict(exclude_unset=True)
            for field, value in update_data.items():
                if hasattr(self.db_obj, field):
                    setattr(self.db_obj, field, value)
        self._update_before_save(data)
        self.db.add(self.db_obj)
        self.db.commit()
        self.db.refresh(self.db_obj)
        self._update_after_save(data)
        return self.db_obj

    def delete(self) -> Base:
        """Delete an existing db object.

        Note that the object must be already fetched.
        """
        if self.db_obj is None:
            raise ValueError('db_obj not set')

        self.db.delete(self.db_obj)
        self.db.commit()
        return self.db_obj

    def delete_bulk(
            self,
            filters: typing.Optional[typing.List],  # Don't know which type is
            filter_combination=None,  # Don't know which type is
    ) -> None:
        """Execute a delete query with filters for bulk deletion of objects.

        :param filters: Search filters as a list.
        :param filter_combination: [Optional] Filter combination function from
                                   SQLAlchemy, such as `or_`, `and_`, etc
                                   (defaults to `and_`).
        """
        query = self.read(
            filters=filters,
            filter_combination=filter_combination,
            return_query=True,
        )
        query.delete()
        self.db.commit()
